from os.path import abspath, dirname, join
from flask import flash, Flask, render_template, abort, request, url_for, redirect, session, g
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import Form
from flask_login import LoginManager, login_user, logout_user, current_user, login_required
from wtforms import fields
from wtforms.ext.sqlalchemy.fields import QuerySelectField

_cwd = dirname(abspath(__file__))

SECRET_KEY = 'secret'
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + join(_cwd, 'mydatabase.db')
SQLALCHEMY_ECHO = True
WTF_CSRF_SECRET_KEY = 'random'

app = Flask(__name__)
app.config.from_object(__name__)

db = SQLAlchemy(app)
	
class User(db.Model):

	__tablename__='logininformation'
	id = db.Column('user_id',db.Integer, primary_key=True)
	username = db.Column('username',db.String(80), unique=True, index=True)
	email = db.Column('email',db.String(120), unique=True, index=True)
	password = db.Column('password',db.String(120))

	def __init__(self, username, email, password):
		self.username = username
		self.email = email
		self.password = password
		
	def is_authenticated(self):
		return True
		
	def is_active(self):
		return True
		
	def is_anonymous(self):
		return False
		
	def get_id(self):
		return unicode(self.id)
		
	def __repr__(self):
		return '<User %r>' % self.username


db.create_all()
	
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'signin'

@login_manager.user_loader
def load_user(id):
	return User.query.get(int(id))
	
@app.route("/")
def main():
	return render_template('index.html')
    
@app.route('/signUp',methods=['GET','POST'])
def signUp():
	if request.method == 'GET':
		return render_template('signup.html')
	u = User(request.form['username'], request.form['email'], request.form['password'])
    
	db.session.add(u)
	db.session.commit()
	return redirect(url_for("signin"))

@app.route('/signin',methods=['POST', 'GET'])
def signin():
	error = None
	if request.method == 'POST':
		if valid_login(request.form['username'],request.form['password']):
			return log_the_user_in(request.form['username'])
		else:
			error = 'Invalid username/password'
	return render_template('signin.html')

	#username = request.form['username']
	#password = request.form['password']
	
	registered_user = User.query.filter_by(username=username, password=password).first()
	if session.get('username'):
	#if registered_user is None:
		return render_template('userHome.html')
	else:
		return render_template('error.html', error='Unauthorized access')
	#login_user(registered_user)
	#flash('Logged in successfully')
	#return redirect('/userHome')

@app.route('/userHome',methods=['GET','POST'])
def userHome():
	return render_template('userHome.html')

@app.route('/logout')
def logout():
	session.pop('username', None)
	return redirect('/')
			
if __name__=="__main__":
	app.debug = True
	app.run()
	
	
